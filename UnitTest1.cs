using Castle.DynamicProxy.Generators.Emitters.SimpleAST;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Appium.Enums;
using OpenQA.Selenium.Appium.Interfaces;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using System.ComponentModel;
using System.Drawing.Text;
using static System.Net.WebRequestMethods;

namespace AppiumClient
{
    public class Tests
    {
        private AppiumDriver<AndroidElement> _driver;
        private static string appium = "http://localhost:4723/wd/hub";
                
        private const string sendto = "lyudmilachaban@gmail.com";
        private const string subject = "TestAppium";
        private const string message = "Hello Appium!";


        [SetUp]

        public void Setup()
        {
            var driverOption = new AppiumOptions();
            driverOption.AddAdditionalCapability(MobileCapabilityType.PlatformName, "Android");
            driverOption.AddAdditionalCapability(MobileCapabilityType.DeviceName, "Android 28");
            driverOption.AddAdditionalCapability(MobileCapabilityType.AutomationName, "UiAutomator2");

            _driver = new AndroidDriver<AndroidElement>(new Uri(appium), driverOption);
        }
        [Test]
        
        public void Test()
        { 

        _driver.FindElementByAccessibilityId("Gmail")
                .Click();
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(40);
            
            _driver.FindElementByAccessibilityId("Compose")
                .Click();


            _driver.FindElementById("android:id/button1")
                .Click();

            _driver.FindElementById("com.google.android.gm:id/to")
                .SendKeys(sendto);
            _driver.FindElementById("com.google.android.gm:id/subject")
                .SendKeys(subject);
 
            _driver.HideKeyboard();
            //_driver.FindElementById("com.google.android.gm:id/body_wrapper")
            //    .Click();
            //_driver.HideKeyboard();
            
            _driver.FindElementByXPath("//hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.RelativeLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RelativeLayout/android.widget.LinearLayout/android.webkit.WebView/android.webkit.WebView/android.view.View").SendKeys(message);

            _driver.FindElementByAccessibilityId("Send")
                .Click();
            _driver.FindElementByAccessibilityId("Open navigation drawer").Click();
           
            _driver.FindElementByXPath("//android.widget.TextView[@text=\"Sent\"]")
                .Click();
            _driver.FindElementByXPath("//android.view.View[contains(@content-desc, \"TestAppium\")]");
          
        }

        [TearDown]
        public void TearDown()
        {
            if (_driver != null)
                _driver.Quit();
        }

        
    }

}

